#include <stdio.h>

/**programa de ejemplo for anidado que imprimne
tabla del 2 a el 5**/
int main(int argc, char const *argv[]) {
  int ini=0;
  int fin=0;
  int j=0;

  printf("Que tabla quieres: \n" );
  scanf("%d", &ini );
  printf("Hasta que tabla: \n" );
  scanf("%d", &fin);

  for ( ini = 0; fin <= fin ; ini++) {
  for (j = 0; j<=10; j=j+1) {


    printf("%d x %d = %d\n", ini ,j, ini*j);
  }

  printf("-----------------------------\n" );
  }
  return 0;
}
