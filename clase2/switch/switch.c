#include <stdio.h>
int main(int argc, char const *argv[]) {
  int opcion=0;
  printf("Selecciona una opción entre 1 y 6: \n");
  scanf("%d", &opcion);

  if (opcion>=1 && opcion<=6) {
    switch (opcion) {
      case 1:
        printf("Hola\n" );
        break;
      case 2:
          printf("Adiós\n" );
          break;
      case 3:
        printf("Hello\n" );
        break;
      case 4:
        printf("Bye\n" );
        break;
      case 5:
        printf("Guten tag\n" );
        break;
      case 6:
        printf("Auf Wiedersehen\n" );
        break;
    }
    
  }else{
    printf("Opción NO válida.\n");
  }

  return 0;
}
